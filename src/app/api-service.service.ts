import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient} from'@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
 baseurl="http://127.0.0.1:8000/user";
  constructor(private http:HttpClient) { }

  getcomments():Observable<any>{
    return this.http.get(this.baseurl)

}

getOnecomments(id):Observable<any>{
    return this.http.get(this.baseurl+'/'+id)

}
updateMovie(com):Observable<any>{
    const body={ 
      name:com.name,
      description:com.description,
      author:com.author,
      comments:com.comments};
    return this.http.put(this.baseurl+'/'+com.id,body)

}
createMovies(com):Observable<any>{
    const body={ 
      name:com.name,
      description:com.description,
      author:com.author,
      comments:com.comments
    };
    return this.http.post(this.baseurl,body)

}
deleteMovies(id):Observable<any>{
    
    return this.http.delete(this.baseurl+'/'+id);

}
}
