import { Component } from '@angular/core';
import{ ApiServiceService } from './api-service.service';
import{HttpClient} from '@angular/common/http'
import {Topic} from './topic'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 topics=[];
 com:any;
 mTopic;
 constructor(private api:ApiServiceService,private http:HttpClient){
this.mTopic={id:4,
            name:"RBA",
            description:"broadcast agents",
            author:"James",
            comments:"no comments"

          };
  
 }
 topic:Topic[];
 ngOnInit(){
  this.api.getcomments()
    .subscribe(
      data=>{
  this.topic=data;

      }
    );
  }


  comClicked(com){

  this.api.getOnecomments(com.id)
.subscribe(
  data=>{
    
    this.mTopic=data;


  }
);
  }

  updateMovies(){

    this.api.updateMovie(this.mTopic)
    .subscribe(
      data=>{
        
        this.mTopic=data;
    
    
      }
    );

  }

  createMovies(){

    this.api.createMovies(this.mTopic)
    .subscribe(
      data=>{
        
        this.topics.push(data)
    
    
      }
    );

  }
  deleteMovies(){

    this.api.deleteMovies(this.mTopic.id)
    .subscribe(
      data=>{
        
        this.topics.push(data)
    
    
      }
    );

 }
 selectedPhoto:File=null;
 onFileSelected(event){
this.selectedPhoto=<File>event.target.files[0];
 }
onUpload(){
  const fd=new FormData();
  fd.append('image',this.selectedPhoto,this.selectedPhoto.name);
this.http.post('htttp://127.0.0.1:8000/topics/images/topics',fd).subscribe(
  res=>{
    console.log(res);
  }
);
}
onUploadlogo(){
  const fd=new FormData();
  fd.append('image',this.selectedPhoto,this.selectedPhoto.name);
this.http.post('htttp://127.0.0.1:8000/topics/images/logos',fd).subscribe(
  res=>{
    console.log(res);
  }
);
}
}
